// dgsa_tsp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <map>


using namespace std;





//input file format: city_index x_position y_position radius
string FILE_PATH = "";
string FILE_NAME = "berlin52.tsp";
int unique_id = 0;// fol pt run_test_suite;

double FLOAT_EQ_CT = 0.00001;
double FLOAT_EQ_CT_CIRCUIT = 0.1;
int MAX_NR_ITERATIONS = 100;
int MAX_NR_ITERATIONS_RBA = 1000;
pair<int, int> EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS;
int MINIMUM_2OPT_MOVEMENTS = 0;
int MINIMUM_2OPT_MOVEMENTS_INCREASE_EVERY_X_ITERATIONS = 0;
double VELOCITY_DECREASE_FACTOR = 1;
double VELOCITY_INCREASE_FACTOR_FOR_ACC = 10;
double VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = 100;
int POPULATION_SIZE = 100;
int K_INITIAL = 20;
int K_END = 0;
double G_INITIAL = 1.2;
double G_END = 0;
int SEED = 1;
int STARTING_CITY_INDEX = 0;
int PERCENTAGE_2_OPT = 100;
int PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = 0;
int PERCENTAGE_IGNORED_POPULATION_WHEN_VEL_CALCULATION = 0;
int NR_TEST_RUNS = 1;
int VELOCITY_RAND_LIMIT_INITIAL_CALCULATION = 20;
int NR_K_MEMBERS_INFLUENCING_SOLUTION = 1;
double SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT = 0;
//0=no; 1=yes
double CALCULATE_MASS_W_MULTIPLICATION_FACTOR = 0;
double MASS_MULTIPLICATION_FACTOR_INDEPENDENT = 2;
int RESTRICTED_CANDIDATE_LIST_SIZE = 1;
int USE_RANDOM_STARTING_POINTS_2_OPT = 0;
int PRINT_PRE_FINAL_2_OPT_VALUES = 0;
int PRINT_FINAL_CIRCUIT = 1;
int PRINT_POST_FINAL_2_OPT_VALUES = 0;
//pt INITIAL_GENERATION_PROCEDURE: 
//0=random; 
//1=nearest neighbour cu random start point; 
//2=random nearest nighbour din cei mai close nr_points/10 vecini
int INITIAL_GENERATION_PROCEDURE = 0;

//mode: 0=basic-length; 1=length+acc+vel(total); 2: length+acc/vel mean 
int INFO_DISPLAY_MODE = 1;

//closeness metric used: also responsible for the make small move operand
//0 = metric = swap/transposition; small_move = swap/transposition
//1 = metric = nr identical edges in conectivity matrix (how many identical connections 2 solutions have);small move = swap/transposition
int SMALL_MOVE_OPERAND_USED = 0;

//multi_target_movement_in_neighbourhood_space selection
// 0 = all used, best solution used last
// 1 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; does not allow double influences
// 2 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; allows double influences
int MTMNS_INFLUENCER_SELECTION = 0;

//0 = random % VELOCITY_RAND_LIMIT_INITIAL_CALCULATION
//1 = random % largest distance between solutions
//2 = fixed at VELOCITY_RAND_LIMIT_INITIAL_CALCULATION
int INITIAL_VELOCITY_GENERATION_MODE = 1;

//0 = uses acc_mean for velocity calculation
//1 = uses acc_total for velocity calculation
int VELOCITY_CALCULATOR_SELECTION = 1;



ofstream outfile;



///simulates an antenna, radius = effective radius, x,y antenna coordinate, x,y _circuit = point on circuit i should reach
struct point_2d
{
	int index;
	double x;
	double y;
	double radius;
	double x_circuit;
	double y_circuit;

	void set_point_2d(int arg_index, double arg_x, double arg_y, double arg_radius, double arg_x_circuit, double arg_y_circuit)
	{
		index = arg_index;
		x = arg_x;
		y = arg_y;
		radius = arg_radius;
		x_circuit = arg_x_circuit;
		y_circuit = arg_y_circuit;
	}

	void copy(point_2d data_in)
	{
		index = data_in.index;
		x = data_in.x;
		y = data_in.y;
		radius = data_in.radius;
		x_circuit = data_in.x_circuit;
		y_circuit = data_in.y_circuit;
	}


	void print()
	{
		cout << index << " " << x << " " << y << " radius= " << radius << " circuit xy= " << x_circuit << " " << y_circuit << endl;
	}


	int check_equals_xy_circuit(point_2d arg_in)
	{
		if ((abs(x_circuit - arg_in.x_circuit) < FLOAT_EQ_CT) && (abs(y_circuit - arg_in.y_circuit)) < FLOAT_EQ_CT)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

};



double calculate_distance_centers(point_2d a, point_2d b)
{
	return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}


double calculate_total_distance_centers(vector <point_2d>  circuit)
{
	double result = 0;
	double distance = 0;
	for (unsigned int i = 0; i < circuit.size() - 1; i++)
	{
		distance = calculate_distance_centers(circuit[i], circuit[i + 1]);
		result = result + distance;
	}
	result = result + calculate_distance_centers(circuit[circuit.size() - 1], circuit[0]);
	return result;
}




double calculate_distance(point_2d a, point_2d b)
{
	return sqrt((a.x_circuit - b.x_circuit)*(a.x_circuit - b.x_circuit) + (a.y_circuit - b.y_circuit)*(a.y_circuit - b.y_circuit));
}


double calculate_total_distance(vector <point_2d>  circuit)
{
	double result = 0;
	double distance = 0;
	for (unsigned int i = 0; i < circuit.size() - 1; i++)
	{
		distance = calculate_distance(circuit[i], circuit[i + 1]);
		result = result + distance;
	}
	result = result + calculate_distance(circuit[circuit.size() - 1], circuit[0]);
	return result;
}











int sgn(double arg_in)
{
	if (arg_in < 0)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}


pair< point_2d, point_2d> calculate_circle_line_intersection(point_2d point_on_line_a, point_2d point_on_line_b, point_2d circle_center)
{
	point_2d solution0;
	point_2d solution1;

	solution0.copy(circle_center);
	solution1.copy(circle_center);

	//from http://mathworld.wolfram.com/Circle-LineIntersection.html
	//place circle origin in (0,0); also move other points
	double x_recentering_movement = circle_center.x;
	double y_recentering_movement = circle_center.y;
	circle_center.x = 0;
	circle_center.y = 0;
	point_on_line_a.x_circuit = point_on_line_a.x_circuit - x_recentering_movement;
	point_on_line_b.x_circuit = point_on_line_b.x_circuit - x_recentering_movement;
	point_on_line_a.y_circuit = point_on_line_a.y_circuit - y_recentering_movement;
	point_on_line_b.y_circuit = point_on_line_b.y_circuit - y_recentering_movement;

	double dx = point_on_line_b.x_circuit - point_on_line_a.x_circuit;
	double dy = point_on_line_b.y_circuit - point_on_line_a.y_circuit;
	double dr = sqrt(dx*dx + dy * dy);
	double det = point_on_line_a.x_circuit*point_on_line_b.y_circuit - point_on_line_b.x_circuit*point_on_line_a.y_circuit;

	double delta = circle_center.radius * circle_center.radius * dr*dr - det * det;

	if (delta < -FLOAT_EQ_CT)// delta < 0
	{
		cout << "ERROR : calculate_circle_line_intersection error 0, no intersection";
	}
	else
	{
		solution0.x_circuit = (det * dy + sgn(dy)*dx*sqrt(delta)) / (dr*dr);
		solution0.y_circuit = (-det * dx + abs(dy)*sqrt(delta)) / (dr*dr);
		if ((delta > -FLOAT_EQ_CT) && ((delta < FLOAT_EQ_CT)))//delta == 0
		{
			solution1.copy(solution0);
		}
		else
		{//if delta> 0
			solution1.x_circuit = (det * dy - sgn(dy)*dx*sqrt(delta)) / (dr*dr);
			solution1.y_circuit = (-det * dx - abs(dy)*sqrt(delta)) / (dr*dr);
		}
	}


	solution0.x_circuit += x_recentering_movement;
	solution0.y_circuit += y_recentering_movement;
	solution1.x_circuit += x_recentering_movement;
	solution1.y_circuit += y_recentering_movement;

	pair<point_2d, point_2d>result(solution0, solution1);

	return result;
}//end calculate circle line intersection


point_2d calculate_perpendicular_point(point_2d point_on_line_a, point_2d point_on_line_b, point_2d third_point)
{
	// a1 x + b1 y + c = 0
	// y = m1 x + m1c;
	point_2d result;
	//calculate coefficients
	double a1, b1, c1, m1, m1c;
	double m2, m2c;
	a1 = point_on_line_a.y_circuit - point_on_line_b.y_circuit;
	b1 = point_on_line_a.x_circuit - point_on_line_b.x_circuit;
	c1 = (point_on_line_a.x_circuit * point_on_line_b.y_circuit) - (point_on_line_b.x_circuit * point_on_line_a.y_circuit);
	m1 = a1 / b1;
	m1c = c1 / b1;

	//perpendicular line
	m2 = -1 / m1;
	m2c = third_point.y_circuit - m2 * third_point.x_circuit;


	//special case for third point on the same line, perpendicular undefined, returns third point itself
	if (abs(m1*third_point.x_circuit + m1c - third_point.y_circuit) < FLOAT_EQ_CT)
	{
		//cout << "error. third point on the same line, perpendicular undefined.";
		return third_point;
	}

	//calculate point; special situations for horizontal/vertical line
	if (abs(point_on_line_a.x_circuit - point_on_line_b.x_circuit) < FLOAT_EQ_CT)
	{
		result.x_circuit = point_on_line_a.x_circuit;
		result.y_circuit = third_point.y_circuit;
		result.x = result.x_circuit;
		result.y = result.y_circuit;
		return result;
	}
	else
	{
		result.x_circuit = ((m2c - m1c) / (m1 - m2));
		result.x = result.x_circuit;
	}

	if (abs(point_on_line_a.y_circuit - point_on_line_b.y_circuit) < FLOAT_EQ_CT)
	{
		result.y_circuit = point_on_line_a.y_circuit;
		result.x_circuit = third_point.x_circuit;
		result.y = result.y_circuit;
		result.x = result.x_circuit;
		return result;
	}
	else
	{
		result.y_circuit = (m1 * result.x_circuit + m1c);
		result.y = result.y_circuit;
	}


	return result;
}


double calculate_distance_point_center_segment(point_2d point_on_line_a, point_2d point_on_line_b, point_2d third_point)
{
	if (calculate_distance(point_on_line_a, point_on_line_b) < FLOAT_EQ_CT) // (line==0) basically => points overlap
	{
		return calculate_distance(point_on_line_a, point_on_line_b);
	}

	point_2d perpendicular_point;
	third_point.x_circuit = third_point.x;
	third_point.y_circuit = third_point.y;
	perpendicular_point = calculate_perpendicular_point(point_on_line_a, point_on_line_b, third_point);


	double min_x, max_x;
	double min_y, max_y;

	min_x = min(point_on_line_a.x_circuit, point_on_line_b.x_circuit);
	max_x = max(point_on_line_a.x_circuit, point_on_line_b.x_circuit);

	min_y = min(point_on_line_a.y_circuit, point_on_line_b.y_circuit);
	max_y = max(point_on_line_a.y_circuit, point_on_line_b.y_circuit);


	if (
		(min_x <= perpendicular_point.x_circuit) && (perpendicular_point.x_circuit <= max_x) &&
		(min_y <= perpendicular_point.y_circuit) && (perpendicular_point.y_circuit <= max_y)
		)
	{
		//return calculate_distance_point_line(point_on_line_a,point_on_line_b,third_point);
		return calculate_distance(third_point, perpendicular_point);
	}
	else
	{
		return min(calculate_distance(point_on_line_a, third_point), calculate_distance(point_on_line_b, third_point));
	}


}




pair<point_2d, point_2d> calculate_acute_angle_bisector(point_2d left_point, point_2d middle_point, point_2d right_point)
{
	//from https://www.quora.com/What-is-the-equation-of-the-acute-angle-bisector-of-two-straight-lines

	// a1 x + b1 y + c1 = 0 ; a2 x + b2 y + c2 = 0
	// y = m1 x + m1c; y = m2 x + m2c
	//calculate coefficients
	double a1, b1, c1;
	double a2, b2, c2;

	a1 = left_point.y_circuit - middle_point.y;
	b1 = middle_point.x - left_point.x_circuit;
	c1 = (left_point.x_circuit * middle_point.y) - (middle_point.x * left_point.y_circuit);

	a2 = middle_point.y - right_point.y_circuit;
	b2 = right_point.x_circuit - middle_point.x;
	c2 = (middle_point.x * right_point.y_circuit) - (right_point.x_circuit * middle_point.y);


	if (sgn(c1) != sgn(c2)) // if signs differ, invert one of the equations
	{
		c2 = -c2;
		b2 = -b2;
		a2 = -a2;
	}
	double s = sqrt(a2*a2 + b2 * b2);
	double t = sqrt(a1*a1 + b1 * b1);

	double a3_0 = a1 * s - a2 * t;
	double b3_0 = b1 * s - b2 * t;
	double c3_0 = c1 * s - c2 * t;

	double a3_1 = a1 * s + a2 * t;
	double b3_1 = b1 * s + b2 * t;
	double c3_1 = c1 * s + c2 * t;

	if (b3_0 == 0)
	{
		b3_0 += FLOAT_EQ_CT;
	}
	if (b3_1 == 0)
	{
		b3_1 += FLOAT_EQ_CT;
	}


	point_2d second_point_from_line0;
	point_2d second_point_from_line1;

	second_point_from_line0.copy(middle_point);
	second_point_from_line0.x = middle_point.x + 1;
	second_point_from_line0.x_circuit = second_point_from_line0.x;
	second_point_from_line0.y = (-a3_0 / b3_0)*second_point_from_line0.x - c3_0 / b3_0;
	second_point_from_line0.y_circuit = second_point_from_line0.y;

	second_point_from_line1.copy(middle_point);
	second_point_from_line1.x = middle_point.x + 1;
	second_point_from_line1.x_circuit = second_point_from_line1.x;
	second_point_from_line1.y = (-a3_1 / b3_1)*second_point_from_line1.x - c3_1 / b3_1;
	second_point_from_line1.y_circuit = second_point_from_line1.y;

	pair<point_2d, point_2d> solutions_from_line0;
	pair<point_2d, point_2d> solutions_from_line1;

	middle_point.x_circuit = middle_point.x;
	middle_point.y_circuit = middle_point.y;
	solutions_from_line0 = calculate_circle_line_intersection(middle_point, second_point_from_line0, middle_point);
	solutions_from_line1 = calculate_circle_line_intersection(middle_point, second_point_from_line1, middle_point);

	solutions_from_line0.first.x = solutions_from_line0.first.x_circuit;
	solutions_from_line0.second.x = solutions_from_line0.second.x_circuit;
	solutions_from_line1.first.x = solutions_from_line1.first.x_circuit;
	solutions_from_line1.second.x = solutions_from_line1.second.x_circuit;
	solutions_from_line0.first.y = solutions_from_line0.first.y_circuit;
	solutions_from_line0.second.y = solutions_from_line0.second.y_circuit;
	solutions_from_line1.first.y = solutions_from_line1.first.y_circuit;
	solutions_from_line1.second.y = solutions_from_line1.second.y_circuit;

	//find best point from all the 4 intersections
	double best_distance_sum;
	int best_line;
	double temp_sum = calculate_distance(left_point, solutions_from_line0.first) + calculate_distance(right_point, solutions_from_line0.first);
	best_distance_sum = temp_sum;
	best_line = 0;
	point_2d second;
	second.copy(solutions_from_line0.first);

	temp_sum = calculate_distance(left_point, solutions_from_line0.second) + calculate_distance(right_point, solutions_from_line0.second);

	if (best_distance_sum > temp_sum)
	{
		best_distance_sum = temp_sum;
		best_line = 0;
		second.copy(solutions_from_line0.second);
	}
	temp_sum = calculate_distance(left_point, solutions_from_line1.first) + calculate_distance(right_point, solutions_from_line1.first);
	if (best_distance_sum > temp_sum)
	{
		best_distance_sum = temp_sum;
		best_line = 1;
		second.copy(solutions_from_line1.first);
	}
	temp_sum = calculate_distance(left_point, solutions_from_line1.second) + calculate_distance(right_point, solutions_from_line1.second);
	if (best_distance_sum > temp_sum)
	{
		best_distance_sum = temp_sum;
		best_line = 1;
		second.copy(solutions_from_line1.second);
	}

	second.x_circuit = second.x;
	second.y_circuit = second.y;




	pair<point_2d, point_2d> result(middle_point, second);
	return result;
}












//varianta cu bisectoarea unghiului , caut punct de la intersectia cerc cu bisectoare
//*
point_2d optimize_points_rubber_band_5(point_2d left_point, point_2d right_point, point_2d middle_point)
{
	point_2d best_point;

	//cazurile de egalitate intre puncte trebuie tratate separat:
	if ((left_point.check_equals_xy_circuit(middle_point) == 1) || (right_point.check_equals_xy_circuit(middle_point) == 1))
	{
		return middle_point;
	}


	//check daca dreapta este inclusa in cerc cu totul
	double distance_point_left_center = calculate_distance(left_point, middle_point);
	double distance_point_right_center = calculate_distance(right_point, middle_point);
	if ((distance_point_left_center < middle_point.radius) && (distance_point_right_center < middle_point.radius))
	{
		best_point.copy(middle_point);
		best_point.x_circuit = right_point.x_circuit;
		best_point.y_circuit = right_point.y_circuit;
		return best_point;
	}


	if (left_point.check_equals_xy_circuit(right_point) == 1)
	{
		middle_point.x_circuit = middle_point.x;
		middle_point.y_circuit = middle_point.y;
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(left_point, middle_point, middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first) + calculate_distance(solutions.first, right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0 < distance_sum1)
		{
			return solutions.first;
		}
		else
		{
			return solutions.second;
		}
	}


	//check daca dreapta trece prin cerc direct => solutie direct punctele de intersectie
		//calc distanta centru cerc <-> segment
			//if smaller 
				//gaseste cele 2 puncte de intersectie cu cercul => dai return
			//if larger => 
				//calculeaza ecuatia bisectoarei unghiului
				//calc intersectia dintre bisectoare si cerc
				//vezi care din cele 2 solutii e mai buna
	double temp_double = calculate_distance_point_center_segment(left_point, right_point, middle_point);

	if (temp_double <= middle_point.radius)
	{
		pair<point_2d, point_2d> solutions(calculate_circle_line_intersection(left_point, right_point, middle_point));
		//check which solution is better
		double distance_sum0 = calculate_distance(left_point, solutions.first) + calculate_distance(solutions.first, right_point);
		double distance_sum1 = calculate_distance(left_point, solutions.second) + calculate_distance(solutions.second, right_point);
		if (distance_sum0 < distance_sum1)
		{
			return solutions.first;
		}
		else
		{
			return solutions.second;
		}
	}
	else//if line does not intersect circle
	{
		//calculate acute angle bisector
		pair<point_2d, point_2d> acute_angle_bisector = calculate_acute_angle_bisector(left_point, middle_point, right_point);

		//acum calculate_acute_angle_bisector da direct punctul de pe cerc
			//asa ca pot da:
		acute_angle_bisector.second.x = acute_angle_bisector.first.x;
		acute_angle_bisector.second.y = acute_angle_bisector.first.y;
		acute_angle_bisector.second.index = acute_angle_bisector.first.index;
		acute_angle_bisector.second.radius = acute_angle_bisector.first.radius;
		return acute_angle_bisector.second;
	}




	return best_point;
}//end point_2d optimize_points_rubber_band_5(point_2d left_point, point_2d right_point, point_2d middle_point)
//*/


vector<point_2d> optimize_circuit_rubber_band(vector<point_2d> initial_circuit, point_2d(*selected_optimizer)(point_2d, point_2d, point_2d))
{
	int nr_iterations = 0;
	vector<point_2d> current_circuit(initial_circuit);
	double current_distance = 0;
	double previous_distance = 0;
	do
	{// until circuit stops improving (or improves too little)	
		previous_distance = calculate_total_distance(current_circuit);
		//error de la 2 la 3
		for (unsigned int i = 0; i < current_circuit.size() - 2; i++)
		{
			current_circuit[i + 1].copy(selected_optimizer(current_circuit[i], current_circuit[i + 2], current_circuit[i + 1]));
		}
		current_circuit[0].copy(selected_optimizer(current_circuit[current_circuit.size() - 1], current_circuit[1], current_circuit[0]));
		current_circuit[current_circuit.size() - 1].copy(selected_optimizer(current_circuit[current_circuit.size() - 2], current_circuit[0], current_circuit[current_circuit.size() - 1]));
		current_distance = calculate_total_distance(current_circuit);

		nr_iterations++;
	} while ((abs(current_distance - previous_distance) > FLOAT_EQ_CT_CIRCUIT) && (nr_iterations <= MAX_NR_ITERATIONS_RBA));

	return current_circuit;
}// end vector<point_2d> optimize_circuit_rubber_band(vector<point_2d> initial_circuit, float stop_limit)













struct solution
{
	vector<point_2d> points;
	int number_unique;
	double length;
	double fitness;
	double q;
	double mass;
	int velocity;//velocity ca numar de miscari de facut
	vector<int> acceleration;//acceleration ca numar de miscari de facut; acceleration[u] = influenta solutiei u asupra lui this; calc doar de la k_best_solutions
	int completed_2opt_for_current_circuit;

	void initialize_all_0()
	{
		points.clear();
		number_unique = 0;
		length = 0;
		fitness = 0;
		q = 0;
		mass = 0;
		velocity = 0;
		acceleration.clear();
		completed_2opt_for_current_circuit = 0;
	}


	void copy(solution arg_in)
	{
		number_unique = arg_in.number_unique;
		points.clear();
		points = arg_in.points;
		length = arg_in.length;
		fitness = arg_in.fitness;
		q = arg_in.q;
		mass = arg_in.mass;
		velocity = arg_in.velocity;
		acceleration.clear();
		acceleration = arg_in.acceleration;
		completed_2opt_for_current_circuit = arg_in.completed_2opt_for_current_circuit;
	}

	void calculate_fitness()
	{
		points = optimize_circuit_rubber_band(points, optimize_points_rubber_band_5);
		length = calculate_total_distance(points);
		fitness = length;

	}

	void calculate_q(double fitness_worst, double fitness_best)
	{
		if (fitness_best == fitness_worst)
		{
			q = FLOAT_EQ_CT;
		}
		else
		{
			q = (fitness - fitness_worst) / (fitness_best - fitness_worst);
		}

	}

	void calculate_mass(double q_sum)
	{
		mass = q / q_sum;//original
		//*
		if (CALCULATE_MASS_W_MULTIPLICATION_FACTOR == 1)
		{
			double a = ceil(points.size() / 50.0);
			double b = (POPULATION_SIZE / 50.0);
			double multiplication_factor = a * b;
			mass = multiplication_factor * q / q_sum; /// XXXX de modificat aici cred ?
		}
		//*/

		mass = MASS_MULTIPLICATION_FACTOR_INDEPENDENT * mass;
	}

	///returns 1 if this is better than other, 0 otherwise
	int check_if_better(solution other)
	{
		if (fitness < other.fitness)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}


	//generate_solution_nearest_neighbour poate sa bag functia aici. vedem


	void swap_points_by_position_in_points_vector(int first_position, int second_position)
	{
		point_2d temp;
		temp.copy(points[second_position]);
		points[second_position].copy(points[first_position]);
		points[first_position].copy(temp);
	}


	void swap_points_by_index(int first_index, int second_index)
	{
		int first_position = 0;
		int second_position = 0;

		for (unsigned int i = 0; i < points.size(); i++)
		{
			if (points[i].index == first_index)
			{
				first_position = i;
				break;
			}
		}
		for (unsigned int i = 0; i < points.size(); i++)
		{
			if (points[i].index == second_index)
			{
				second_position = i;
				break;
			}
		}
		swap_points_by_position_in_points_vector(first_position, second_position);
	}


};//end struct solution 










void print_circuit(vector <point_2d>  circuit)
{
	cout << "total circuit length = " << calculate_total_distance(circuit) << endl;
	cout << "total circuit length center = " << calculate_total_distance_centers(circuit) << endl;
	for (unsigned int i = 0; i < circuit.size(); i++)
	{
		circuit[i].print();
	}
	cout << endl << endl << endl << endl << endl;
}





bool sort_function_ascending_distance(pair<double, point_2d> a, pair<double, point_2d> b)
{
	return a.first < b.first;
}

bool sort_function_descending_distance(pair<double, point_2d> a, pair<double, point_2d> b)
{
	return a.first > b.first;
}

bool sort_function_ascending_fitness(solution a, solution b)
{
	return a.fitness < b.fitness;
}

bool sort_function_descending_fitness(solution a, solution b)
{
	return a.fitness > b.fitness;
}




solution generate_solution_nearest_neighbour(vector<point_2d> initial_circuit)
{
	int nr_random;
	nr_random = rand() % initial_circuit.size();

	solution solution0;
	solution0.points.push_back(initial_circuit[nr_random]);
	initial_circuit.erase(initial_circuit.begin() + nr_random);

	while (initial_circuit.size() != 0)
	{
		//look for nearest neighbour
		int best_index = 0;
		double best_distance = 999999999;
		for (unsigned int i = 0; i < initial_circuit.size(); i++)
		{
			double temp_distance = calculate_distance_centers(solution0.points[solution0.points.size() - 1], initial_circuit[i]);
			if (temp_distance < best_distance)
			{
				best_distance = temp_distance;
				best_index = i;
			}

		}
		//add to solution and delete from initial
		solution0.points.push_back(initial_circuit[best_index]);
		initial_circuit.erase(initial_circuit.begin() + best_index);
	}


	return solution0;
}




solution generate_solution_random(vector<point_2d> initial_circuit)
{
	int nr_random;
	nr_random = rand() % initial_circuit.size();

	solution solution0;

	while (initial_circuit.size() != 0)
	{
		nr_random = rand() % initial_circuit.size();
		solution0.points.push_back(initial_circuit[nr_random]);
		initial_circuit.erase(initial_circuit.begin() + nr_random);
	}


	return solution0;
}



solution generate_solution_nearest_neighbour_from_closest(vector<point_2d> initial_circuit, int nr_considered_neighbours)
{
	int nr_random;
	nr_random = rand() % initial_circuit.size();

	solution solution0;
	solution0.points.push_back(initial_circuit[nr_random]);
	initial_circuit.erase(initial_circuit.begin() + nr_random);

	int k = nr_considered_neighbours;
	vector<pair<double, point_2d>> temp;

	while (initial_circuit.size() != 0)
	{
		temp.clear();
		for (int i = 0; i < initial_circuit.size(); i++)
		{
			pair<double, point_2d> a;
			a.second.copy(initial_circuit[i]);
			a.first = calculate_distance_centers(solution0.points[solution0.points.size() - 1], initial_circuit[i]);
			temp.push_back(a);
		}

		if (temp.size() < 2)
		{
			//do nothing
		}
		else
		{
			sort(temp.begin(), temp.end(), sort_function_ascending_distance);
		}


		nr_random = rand() % (k < temp.size() ? k : temp.size());

		int desired_position_of_selected_point = 0;

		for (int j = 0; j < initial_circuit.size(); j++)//search for desired_index_of_selected_point
		{
			if (temp[nr_random].second.index == initial_circuit[j].index)
			{
				desired_position_of_selected_point = j;
				break;
			}
		}

		//add to solution and delete from temp
		solution0.points.push_back(initial_circuit[desired_position_of_selected_point]);
		initial_circuit.erase(initial_circuit.begin() + desired_position_of_selected_point);
	}


	return solution0;
}


vector<solution> generate_initial_population(vector<point_2d> initial_circuit, int population_size)
{
	vector<solution> population;
	for (int i = 0; i < population_size; i++)
	{
		switch (INITIAL_GENERATION_PROCEDURE)
		{
		case 0: //random generation
		{
			population.push_back(generate_solution_random(initial_circuit));
			break;
		}
		case 1: //nearest neighbour with random start
		{
			population.push_back(generate_solution_nearest_neighbour(initial_circuit));
			break;
		}
		case 2: //nearest neighbour selected randomly from closest neighbours with random start
		{
			population.push_back(generate_solution_nearest_neighbour_from_closest(initial_circuit, RESTRICTED_CANDIDATE_LIST_SIZE));
			break;
		}

		}
		population[i].number_unique = i;
		population[i].completed_2opt_for_current_circuit = 0;
	}
	return population;
}





vector<int> invert_permutation(vector<int> arg_in0)
{
	vector<int> result;
	result.resize(arg_in0.size());

	//f: v[i]=>x ;    f-1: v[x]=>i

	for (unsigned int i = 0; i < result.size(); i++)
	{
		result[arg_in0[i]] = i;
	}

	return result;
}

/// result = arg_in0 o arg_in1; 
vector<int> compose_permutations(vector<int> arg_in0, vector<int> arg_in1)
{
	vector<int> result;
	result.resize(arg_in0.size());

	//f: v[i]=>x ;    f-1: v[x]=>i

	for (unsigned int i = 0; i < result.size(); i++)
	{
		result[i] = arg_in1[arg_in0[i]];
	}

	return result;

}



int count_permutation_cycle_decomposition(vector<int> arg_in0)
{
	int result = 0;
	vector<int> visited;
	visited.resize(arg_in0.size(), 0);

	for (unsigned int i = 0; i < visited.size(); i++)
	{
		if (visited[i] == 0)//if not yet visited
		{
			int j = i;
			result = result + 1;
			while (visited[j] != 1)//not reached end of cycle (visited the same point twice), make another move
			{
				visited[j] = 1;
				j = arg_in0[j];
			}
		}

	}

	return result;
}



int count_minimum_number_of_transpositions_between_permutations(vector<int> permutation0, vector<int> permutation1)
{
	int result = 0;
	result = permutation0.size() - count_permutation_cycle_decomposition(compose_permutations(invert_permutation(permutation1), permutation0));


	return result;
}







vector<vector<int>> make_matrix_from_route(vector<int> route)
{
	vector<vector<int>> result;
	//fac nxn matrice goala
	vector<int> temp(route.size(), 0);

	for (int i = 0; i < route.size(); i++)
	{
		result.push_back(temp);
	}

	// pun 1 unde trebuie
	for (int i = 0; i < route.size(); i++)
	{
		int next_point_index;
		int current_point_index;
		int previous_point_index;

		current_point_index = route[i];
		next_point_index = route[(i + 1) % route.size()];
		if (i == 0)
		{
			previous_point_index = route[route.size() - 1];
		}
		else
		{
			previous_point_index = route[(i - 1)];
		}
		result[current_point_index][previous_point_index] = 1;
		result[current_point_index][next_point_index] = 1;
	}

	return result;
}



vector<vector<int>> make_matrix_from_route(vector<point_2d> arg_route)
{
	vector<int> route;

	for (int i = 0; i < arg_route.size(); i++)
	{
		route.push_back(arg_route[i].index);
	}

	return make_matrix_from_route(route);
}



vector<vector<int>> sub_matrix(vector<vector<int>> a, vector<vector<int>> b)
{
	vector<vector<int>> result;

	//check sizes // lene sa implementez


	//actual add
	vector<int> temp(a.size(), 0);

	for (int i = 0; i < a.size(); i++)
	{
		for (int j = 0; j < a[i].size(); j++)
		{
			temp[j] = a[i][j] - b[i][j];
		}
		result.push_back(temp);
	}



	return result;
}


int count_nr_of(int counted_element, vector<vector<int>> matrix)
{
	int result = 0;
	for (int i = 0; i < matrix.size(); i++)
	{
		for (int j = 0; j < matrix[i].size(); j++)
		{
			if (matrix[i][j] == counted_element)
			{
				result = result + 1;
			}
		}
	}
	return result;
}



int calculate_smns_distance_min(solution start_solution, solution destination_solution)
{
	int result = 0;

	switch (SMALL_MOVE_OPERAND_USED)
	{
	case(0):
	{
		vector<int> initial_permutation;
		vector<int> final_permutation;

		for (unsigned int i = 0; i < start_solution.points.size(); i++)
		{
			initial_permutation.push_back(start_solution.points[i].index);
			final_permutation.push_back(destination_solution.points[i].index);
		}

		result = count_minimum_number_of_transpositions_between_permutations(initial_permutation, final_permutation);
		break;
	}
	case(1):
	{
		//bigger distance, worse solution

		// idea: make conectivity matrix, sub them, count nr of conections that appear in one and not the other
		//*
		vector<vector<int>> a;
		vector<vector<int>> b;
		vector<vector<int>> c;
		a = make_matrix_from_route(start_solution.points);
		b = make_matrix_from_route(destination_solution.points);
		c = sub_matrix(a, b);
		result = count_nr_of(1, c) / 2;
		break;
	}//end case 1
	case(2):
	{
		vector<int> initial_permutation;
		vector<int> final_permutation;

		for (unsigned int i = 0; i < start_solution.points.size(); i++)
		{
			initial_permutation.push_back(start_solution.points[i].index);
			final_permutation.push_back(destination_solution.points[i].index);
		}

		result = count_minimum_number_of_transpositions_between_permutations(initial_permutation, final_permutation);
		break;
	}
	case(3):
	{
		vector<int> initial_permutation;
		vector<int> final_permutation;

		for (unsigned int i = 0; i < start_solution.points.size(); i++)
		{
			initial_permutation.push_back(start_solution.points[i].index);
			final_permutation.push_back(destination_solution.points[i].index);
		}

		result = count_minimum_number_of_transpositions_between_permutations(initial_permutation, final_permutation);
		break;
	}
	case(4):
	{
		vector<vector<int>> a;
		vector<vector<int>> b;
		vector<vector<int>> c;
		a = make_matrix_from_route(start_solution.points);
		b = make_matrix_from_route(destination_solution.points);
		c = sub_matrix(a, b);
		result = count_nr_of(1, c) / 2;
		//result = floor(1.0*result / 4);
		break;
	}
	default:
	{
		cout << "ERROR improper small move selected";
		break;
	}

	}



	return result;
}




double calculate_distance_between_solutions(solution start_solution, solution end_solution)
{
	double result = 0;

	result = calculate_smns_distance_min(start_solution, end_solution);

	return result;
}


///initial velocity is generated randomly
void generate_initial_velocity(vector<solution> & solutions)
{

	switch (INITIAL_VELOCITY_GENERATION_MODE)
	{
	case(0)://random limited by VELOCITY_RAND_LIMIT_INITIAL_CALCULATION
	{
		for (unsigned int i = 0; i < solutions.size(); i++)
		{
			solutions[i].velocity = 0;
			double nr_random = 0;
			nr_random = rand() % VELOCITY_RAND_LIMIT_INITIAL_CALCULATION;
			solutions[i].velocity = int(floor(nr_random));
			solutions[i].velocity = solutions[i].velocity / VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION;
		}
		break;
	}//end case 0 
	case(1)://random limited by max distance between solutions
	{
		//calculate max distance between any two solutions
		double max_distance;
		max_distance = calculate_distance_between_solutions(solutions[0], solutions[0]);

		for (unsigned int i = 0; i < solutions.size(); i++)
		{
			for (unsigned int j = 0; j < solutions.size(); j++)
			{
				double temp_distance = 0;
				temp_distance = calculate_distance_between_solutions(solutions[i], solutions[j]);
				if (max_distance < temp_distance)
				{
					max_distance = temp_distance;
				}
			}
		}
		
		//actual velocity calculation
		for (unsigned int i = 0; i < solutions.size(); i++)
		{
			solutions[i].velocity = 0;
			double nr_random = 0;
			if (max_distance != 0)
			{
				nr_random = rand() % int(max_distance);

			}
			else
			{
				nr_random = 0;
			}
			solutions[i].velocity = int(floor(nr_random));
			solutions[i].velocity = solutions[i].velocity / VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION;
		}
		break;
	}//end case 1
	case(2)://fixed at VELOCITY_RAND_LIMIT_INITIAL_CALCULATION
	{
		for (unsigned int i = 0; i < solutions.size(); i++)
		{
			solutions[i].velocity = 0;
			double nr_random = 0;
			nr_random = VELOCITY_RAND_LIMIT_INITIAL_CALCULATION;
			solutions[i].velocity = int(floor(nr_random));
			solutions[i].velocity = solutions[i].velocity / VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION;
		}
		break;
	}//end case 2

	}







}





void calculate_fitness(vector<solution> & population)
{
	for (unsigned int i = 0; i < population.size(); i++)
	{
		population[i].calculate_fitness();
	}
}

void calculate_mass(vector<solution> & population)
{
	solution fitness_best;
	solution fitness_worst;
	double q_sum = 0;

	fitness_best.fitness = 999999999;
	fitness_worst.fitness = 0;
	for (unsigned int i = 0; i < population.size(); i++)
	{

		if (fitness_best.check_if_better(population[i]) == 0)//if best is not better than current
		{
			fitness_best.fitness = population[i].fitness;
		}
		if (fitness_worst.check_if_better(population[i]) == 1)//if worst is better than current
		{
			fitness_worst.fitness = population[i].fitness;
		}
	}

	for (unsigned int i = 0; i < population.size(); i++)
	{
		population[i].calculate_q(fitness_worst.fitness, fitness_best.fitness);
		q_sum = q_sum + population[i].q;
	}

	for (unsigned int i = 0; i < population.size(); i++)
	{
		population[i].calculate_mass(q_sum);
	}


}



int stop_condition(int nr_iterations)
{
	if (MAX_NR_ITERATIONS > nr_iterations)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

///implemented linear decreasing
double calculate_g(double g_initial, double g_end, int nr_iterations)
{
	double result = 0;
	double step = (g_initial - g_end) / MAX_NR_ITERATIONS;
	result = g_end + (MAX_NR_ITERATIONS - nr_iterations) * step;
	return result;
}

///implemented linear decreasing
int calculate_k(int k_initial, int k_end, int nr_iterations)
{
	int result;
	double step = (1.0* k_initial - k_end) / MAX_NR_ITERATIONS;
	result = ceil(k_end + (MAX_NR_ITERATIONS - nr_iterations) * step);
	return result;
}




vector<solution> find_k_best(vector<solution> population, int k)
{
	vector<solution> result;
	if (population.size() < 2)
	{
		//do nothing
	}
	else
	{
		sort(population.begin(), population.end(), sort_function_ascending_fitness);//ascending fitness. lower is better (fitness==tour distance)

	}
	vector<solution>::iterator it = result.begin();
	result.insert(it, population.begin(), population.begin() + k);

	return result;
}





int calculate_smns_distance_max(solution start_solution, solution end_solution)
{
	//if possible. nu e chiar corect dar asta e // XXXX poate ask
	return start_solution.points.size() - 1;
}


double normalize_in_interval(double interval_start, double interval_end, double value, solution start_solution, solution end_solution)
{
	return interval_start + (value / (2.0 * calculate_smns_distance_max(start_solution, end_solution)));
}


double calculate_acceleration_from_solution(solution influenced_solution, solution  influencing_solution, double g)
{
	// acc = Fij/Mi => direct mai jos

	//double Fij = ((double)(rand() % 1000) / 1000) * g * ((influenced_solution.mass * influencing_solution.mass) / (normalize_in_interval(0.5,1,Rij, influenced_solution, influencing_solution))) * Rij;
	double acc = 0;
	int Rij = calculate_smns_distance_min(influenced_solution, influencing_solution);
	double rand_number = ((double)(rand() % 1000)) / 1000.0;
	double NRij = normalize_in_interval(0.5, 1, Rij, influenced_solution, influencing_solution);
	acc = rand_number * g * Rij *
		(influencing_solution.mass / NRij);
	//double acc_multiplication_factor = influenced_solution.points.size()*POPULATION_SIZE/;

	return acc;
}



void calculate_acceleration(solution & population_member, vector<solution>  k_best_solutions, double g)
{
	population_member.acceleration.clear();
	for (unsigned int i = 0; i < k_best_solutions.size(); i++)
	{
		population_member.acceleration.push_back(int(calculate_acceleration_from_solution(population_member, k_best_solutions[i], g)));
	}


}



void calculate_acceleration_k_best(vector<solution> & population, vector<solution> k_best_solutions, double g)
{
	for (unsigned int i = 0; i < population.size(); i++)
	{
		calculate_acceleration(population[i], k_best_solutions, g);

	}


}




solution make_small_move(solution starting_solution, solution target_solution)
{

	solution result;
	solution temp_result;
	int distance_between_start_target_solutions = int(calculate_distance_between_solutions(starting_solution, target_solution));


	switch (SMALL_MOVE_OPERAND_USED)
	{
	case(0):
	{
		// implementation 0: swap operation between 2 points in tour
		// problem: can freeze: ex: target= 1 2 3 4 5; start = 1 4 2 3 5; guess its ok ,reached local minimum.
		// nu caut steps care sa produca longest path neaparat i guess. 
		//*/
		int rand_nr_1 = rand() % starting_solution.points.size();
		int rand_nr_2 = rand() % starting_solution.points.size();

		int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
		int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

		for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)
		{
			for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
			{
				result.copy(starting_solution);
				result.swap_points_by_index(i%starting_solution.points.size(), j%starting_solution.points.size());
				if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
				{
					result.completed_2opt_for_current_circuit = 0;
					return result;
				}
			}
		}
		//*/
		break;
	}//end case 0
	case(1):
	{
		// implementation 1: swap operation between 2 points in tour + metric nr of different connections
		//*/
		int rand_nr_1 = rand() % starting_solution.points.size();
		int rand_nr_2 = rand() % starting_solution.points.size();

		int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
		int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

		for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)
		{
			for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
			{
				result.copy(starting_solution);
				result.swap_points_by_index(i%starting_solution.points.size(), j%starting_solution.points.size());
				if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
				{
					result.completed_2opt_for_current_circuit = 0;
					return result;
				}
			}
		}
		//*/
		break;
	}//end case 1

	case(2): //same as case 0 but also takes fitness into account. does not allow big fitness degradations if possible 
	{
		int rand_nr_1 = rand() % starting_solution.points.size();
		int rand_nr_2 = rand() % starting_solution.points.size();

		int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
		int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

		for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)
		{
			for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
			{
				result.copy(starting_solution);
				result.swap_points_by_index(i%starting_solution.points.size(), j%starting_solution.points.size());
				if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
				{
					temp_result.copy(result);
					temp_result.completed_2opt_for_current_circuit = 0;
					temp_result.calculate_fitness();

					double points_impact_percentage = 100.0 / temp_result.points.size();
					double length_relative_error; //length modification that just happened; 

					length_relative_error = ((temp_result.length - starting_solution.length) / starting_solution.length) * 100;

					if (SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT > length_relative_error / points_impact_percentage)
					{	//pt eroare negativa e de bine, inseamna improvement
						result.completed_2opt_for_current_circuit = 0;
						return result;
					}
				}
			}
		}
		return temp_result;//if all steps degrade fitness a lot, return last one; //make a bad move
		break;
	}
	case(3): //same as case 2 but refuses to make a move that degrades fitness a lot
	{
		int rand_nr_1 = rand() % starting_solution.points.size();
		int rand_nr_2 = rand() % starting_solution.points.size();

		int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
		int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

		for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)
		{
			for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
			{
				result.copy(starting_solution);
				result.swap_points_by_index(i%starting_solution.points.size(), j%starting_solution.points.size());
				if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
				{
					temp_result.copy(result);
					temp_result.completed_2opt_for_current_circuit = 0;
					temp_result.calculate_fitness();

					double points_impact_percentage = 100 / temp_result.points.size();
					double length_relative_error; //length modification that just happened; 

					length_relative_error = ((temp_result.length - starting_solution.length) / starting_solution.length) * 100;



					if (SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT > length_relative_error / points_impact_percentage)
					{
						result.completed_2opt_for_current_circuit = 0;
						return result;
					}
				}
			}
		}
		return starting_solution;//if all steps degrade fitness a lot, dont make move
		break;
	}
	case(4): //4 = metric = nr identical edges; small move = remove point and reinsert in better position
	{
		int rand_nr_1 = rand() % starting_solution.points.size();
		int rand_nr_2 = rand() % starting_solution.points.size();

		int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
		int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

		for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)//take point i. place in position j
		{
			for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
			{
				result.copy(starting_solution);
				point_2d removed_point = result.points[i%starting_solution.points.size()];
				result.points.erase(result.points.begin() + (i%starting_solution.points.size()));
				result.points.insert(result.points.begin() + (j%starting_solution.points.size()), removed_point);
				if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
				{
					result.completed_2opt_for_current_circuit = 0;
					return result;
				}
			}
		}
		break;
	}
	default:
	{
		cout << "ERROR make small mover operator improperly selected";
		break;
	}

	}//endcase




	// implementation 2: scot point din tour si insert in pozitie mai buna
	/*/
	for (int i = 0; i < starting_solution.points.size(); i++)
	{
		result.copy(starting_solution);
		point_2d temp;
		temp.copy(result.points[i]);
		result.points.erase(result.points.begin() + i);
		for (int j = 0; j < starting_solution.points.size(); j++)
		{
			result.points.insert(result.points.begin() + j, temp);
			if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
			{
				return result;
			}
			else
			{
				result.points.erase(result.points.begin() + j);
			}
		}

	}
	cout << "make_small_move error 1: should never reach this point";

	//*/

	return result;
}


solution make_simple_movement_in_neighbourhood_space(solution starting_solution, solution target_solution, int movement_length)
{
	solution result;
	result.copy(starting_solution);
	for (int i = 0; i < movement_length; i++)
	{
		result = make_small_move(result, target_solution);
	}
	return result;
}



solution solve_multi_target_movement_in_neighbourhood_space(solution starting_solution, vector<solution> k_best_solutions)
{//nr de target solutions e dat de k_best_solutions.size() si movement lengths e dat de acceleratie care e incorporata in solutie "starting_solution.acceleration"
	//k_best_solutions already sorted descending dupa fitness. o sa iau worst solution makes first move
	solution result;
	result.copy(starting_solution);



	switch (MTMNS_INFLUENCER_SELECTION)
	{
	case(0)://all used, best solution used last
	{
		for (int i = k_best_solutions.size() - 1; i >= 0; i--)
		{
			result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[i], result.acceleration[i]);
		}
		break;
	}
	case(1)://only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; does not allow double influences
	{

		for (int i = 0; i < NR_K_MEMBERS_INFLUENCING_SOLUTION; i++)
		{
			if (k_best_solutions.size() == 0)
			{
				return result;
			}
			else
			{
				int nr_random = rand() % k_best_solutions.size();
				result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[nr_random], result.acceleration[nr_random]);
				k_best_solutions.erase(k_best_solutions.begin() + nr_random);
			}
		}
		break;
	}
	case(2)://only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; allows double influences
	{
		for (int i = 0; (i < NR_K_MEMBERS_INFLUENCING_SOLUTION) && (i < k_best_solutions.size()); i++)
		{
			int nr_random = rand() % k_best_solutions.size();
			result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[nr_random], result.acceleration[nr_random]);
		}
		break;
	}
	case(3)://same as 2 but makes sure some movement is made if possible
	{
		for (int i = 0; (i < NR_K_MEMBERS_INFLUENCING_SOLUTION) && (i < k_best_solutions.size()); i++)
		{

			int nr_random = rand() % k_best_solutions.size();
			if (result.acceleration[nr_random] == 0)
			{
				k_best_solutions.erase(k_best_solutions.begin() + nr_random);
				result.acceleration.erase(result.acceleration.begin() + nr_random);
				i--;
				continue;
			}
			result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[nr_random], result.acceleration[nr_random]);
		}
		break;
	}
	default:
	{
		break;
	}
	}//end case

	result.acceleration = starting_solution.acceleration;

	return result;
}



solution two_opt_swap(solution starting_solution, int first_limit, int second_limit)
{
	solution result;
	result.copy(starting_solution);//aici optimizare: sa fac functie de copy doar pt id si dastea
	result.points.clear();

	for (int i = 0; i < first_limit; i++)
	{
		result.points.push_back(starting_solution.points[i]);
	}
	for (int i = second_limit - 1; i >= first_limit; i--)
	{
		result.points.push_back(starting_solution.points[i]);
	}
	for (unsigned int i = second_limit; i < starting_solution.points.size(); i++)
	{
		result.points.push_back(starting_solution.points[i]);
	}



	return result;
}



solution solve_modified_2_opt(solution starting_solution, int iteration)
{
	double random_nr = (rand() % 1000) / 1000.0;
	int movement_length;
	int extra_movement = 0;
	if (iteration % EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.first == 0)
	{
		extra_movement = MINIMUM_2OPT_MOVEMENTS + EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.second;
	}
	else
	{
		extra_movement = MINIMUM_2OPT_MOVEMENTS;
	}
	extra_movement = extra_movement + int(floor(iteration / MINIMUM_2OPT_MOVEMENTS_INCREASE_EVERY_X_ITERATIONS));
	movement_length = extra_movement + int(random_nr * starting_solution.velocity);
	int movements_done = 0;

	if (movement_length == 0)
	{
		return starting_solution;
	}
	solution result;
	result.copy(starting_solution);

	int n = starting_solution.points.size();
	int rand_i;


	if (result.completed_2opt_for_current_circuit == 1)//if 2opt already done fully, dont waste time
	{
		return result;
	}

algorithm_start:
	result.points = optimize_circuit_rubber_band(result.points, optimize_points_rubber_band_5);
	double best_distance = calculate_total_distance(result.points);
	if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
	{
		rand_i = rand() % (n - 1);
	}
	else
	{
		rand_i = 0;
	}
	for (unsigned int i = 0; i < starting_solution.points.size() - 1; i++)
	{
		int rand_k;
		if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
		{
			rand_k = rand() % (n - 1);
		}
		else
		{
			rand_k = 0;
		}
		int first_limit = (i + rand_i) % (n - 1);
		for (unsigned int k = 0; k < starting_solution.points.size() - first_limit; k++)
		{
			int second_limit = first_limit + ((k + rand_k) % n) % (n - first_limit);

			solution temp = two_opt_swap(result, first_limit, second_limit);
			temp.points = optimize_circuit_rubber_band(temp.points, optimize_points_rubber_band_5);
			double temp_distance = calculate_total_distance(temp.points);
			if (temp_distance < best_distance)
			{
				result.copy(temp);
				best_distance = temp_distance;
				movements_done = movements_done + 1;
				if (movements_done >= movement_length)
				{
					return result;
				}
				goto algorithm_start;
			}
		}
	}

	result.completed_2opt_for_current_circuit = 1;
	return result;
}


//complete 2 opt solver until no improvements can be made
solution solve_2_opt(solution starting_solution)
{
	solution result;
	result.copy(starting_solution);

	int n = starting_solution.points.size();
	int rand_i;


	if (result.completed_2opt_for_current_circuit == 1)//if 2opt already done fully, dont waste time
	{
		return result;
	}

algorithm_start:
	result.points = optimize_circuit_rubber_band(result.points, optimize_points_rubber_band_5);
	double best_distance = calculate_total_distance(result.points);
	if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
	{
		rand_i = rand() % (n - 1);
	}
	else
	{
		rand_i = 0;
	}
	for (unsigned int i = 0; i < starting_solution.points.size() - 1; i++)
	{
		int rand_k;
		if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
		{
			rand_k = rand() % (n - 1);
		}
		else
		{
			rand_k = 0;
		}
		int first_limit = (i + rand_i) % (n - 1);
		for (unsigned int k = 0; k < starting_solution.points.size(); k++)
		{
			int second_limit = first_limit + ((k + rand_k) % n) % (n - first_limit);
			solution temp = two_opt_swap(result, (i + rand_i) % (n - 1), (k + rand_k) % n);
			temp.points = optimize_circuit_rubber_band(temp.points, optimize_points_rubber_band_5);
			double temp_distance = calculate_total_distance(temp.points);
			if (temp_distance < best_distance)
			{
				result.copy(temp);
				best_distance = temp_distance;
				goto algorithm_start;
			}
		}
	}
	result.completed_2opt_for_current_circuit = 1;
	return result;
}




void calculate_velocity(vector<solution> & population)
{
	switch (VELOCITY_CALCULATOR_SELECTION)
	{
	case(0)://uses acc_mean for velocity calculation
	{
		for (unsigned int i = 0; i < population.size(); i++)
		{
			double acceleration_mean = 0;
			for (unsigned int j = 0; j < population[i].acceleration.size(); j++)
			{
				acceleration_mean = acceleration_mean + population[i].acceleration[j];
			}
			acceleration_mean = acceleration_mean / population[i].acceleration.size();
			double random_number = rand();
			random_number = ((int(random_number) % 1000) / 1000.0);
			population[i].velocity = int(random_number * population[i].velocity + acceleration_mean * VELOCITY_INCREASE_FACTOR_FOR_ACC);
			population[i].velocity = population[i].velocity*1.0 / VELOCITY_DECREASE_FACTOR;

		}
		break;
	}
	case(1)://uses total acc for velocity_calculation
	{
		for (unsigned int i = 0; i < population.size(); i++)
		{
			double acceleration_total = 0;
			for (unsigned int j = 0; j < population[i].acceleration.size(); j++)
			{
				acceleration_total = acceleration_total + population[i].acceleration[j];
			}
			double random_number = rand();
			random_number = ((int(random_number) % 1000) / 1000.0);
			population[i].velocity = int(random_number * population[i].velocity + acceleration_total * VELOCITY_INCREASE_FACTOR_FOR_ACC);
			population[i].velocity = population[i].velocity*1.0 / VELOCITY_DECREASE_FACTOR;

		}
		break;
	}
	default:
	{
		cout << "ERROR improper velocity calculator selected";
		break;
	}

	}



}


vector<point_2d> shift_points_right(vector<point_2d> what_im_shifting, int how_much)
{
	vector<point_2d> result;
	int n = what_im_shifting.size();
	for (int i = 0; i < n; i++)
	{
		result.push_back(what_im_shifting[(n - how_much + i) % n]);
	}

	return result;
}


int find_position_of_index(solution arg_solution, int index_to_find)
{
	unsigned int starting_city_index_position = 0;
	for (starting_city_index_position = 0; starting_city_index_position < arg_solution.points.size(); starting_city_index_position++)
	{
		if (index_to_find == arg_solution.points[starting_city_index_position].index)
		{
			return starting_city_index_position;
		}
	}
	return starting_city_index_position;
}


vector<solution> shift_population(vector<solution> population, int starting_city_index)
{
	vector<solution> result = population;
	for (unsigned int i = 0; i < population.size(); i++)
	{
		result[i].points.clear();
		int temp = 0;
		temp = find_position_of_index(population[i], starting_city_index);//find position of starting_city_index inside each solution
		result[i].points = shift_points_right(population[i].points, (population[i].points.size() - temp));//shift all by that amount so that starting_city_index is first
	}
	return result;
}

solution find_best(vector<solution> population)
{
	solution result;
	result.copy(population[0]);
	for (unsigned int i = 0; i < population.size(); i++)
	{
		if (result.check_if_better(population[i]) == 0)
		{
			result.copy(population[i]);
		}
	}
	return result;
}



void print_best(vector< solution> population)
{
	cout << "best distance = " << calculate_total_distance(find_best(population).points);
}



void display_infos(vector <solution>population, int nr_iterations, int mode)
{
	double total_length_mean = 0;
	double mean_acceleration = 0;
	double total_completed_2opt_circuits = 0;
	double mean_velocity = 0;
	for (unsigned int i = 0; i < population.size(); i++)
	{
		total_length_mean += calculate_total_distance(population[i].points);
		for (int j = 0; j < population[i].acceleration.size(); j++)
		{
			mean_acceleration = mean_acceleration + population[i].acceleration[j];
		}
		mean_velocity = mean_velocity + population[i].velocity;
		total_completed_2opt_circuits += population[i].completed_2opt_for_current_circuit;
	}
	total_length_mean = total_length_mean / population.size();


	//std::cout << fixed;
	//std::cout << setprecision(4);
	switch (mode)
	{
	case 0:
	{
		cout << "iteration: " << nr_iterations << " of " << MAX_NR_ITERATIONS << " total_length_mean = " << total_length_mean << " ";
		print_best(population);
		cout << endl;
		cout << endl << endl;
		break;
	}
	case 1:
	{
		cout << "iteration: " << nr_iterations << " of " << MAX_NR_ITERATIONS << " total_length_mean = " << total_length_mean << " ";
		print_best(population);
		cout << endl;
		cout << "total acceleration = " << mean_acceleration << " total velocity = " << mean_velocity;
		cout << " total completed_2opt circuits = " << total_completed_2opt_circuits;
		cout << endl << endl;
		break;
	}
	case 2:
	{
		cout << "iteration: " << nr_iterations << " of " << MAX_NR_ITERATIONS << " total_length_mean = " << total_length_mean << " ";
		print_best(population);
		cout << endl;
		mean_acceleration = mean_acceleration / population[0].acceleration.size();
		mean_acceleration = mean_acceleration / population.size();
		mean_velocity = mean_velocity / population.size();
		cout << "mean acceleration = " << mean_acceleration << " mean velocity = " << mean_velocity;
		cout << " total completed_2opt circuits = " << total_completed_2opt_circuits;
		cout << endl << endl;
		break;
	}
	case 3:
	{
		//no display
		break;
	}
	}



}



void print_population(vector<solution> population)
{
	for (int i = 0; i < population.size(); i++)
	{
		print_circuit(population[i].points);
	}


}







/// initial circuit = circuit as read from file
/// s = population size (nr searcher agents)
/// k/g iniital - k for k_best selection, g for gravitational constant. both decrease over time
solution solve_dgsa_tsp(vector<point_2d> initial_circuit, int population_size, int k_initial, int k_end, double g_initial, double g_end)
{
	double g = g_initial;
	double k = k_initial;
	vector <solution> k_best_solutions;
	solution result;
	solution best_ever;
	//initialization phase
	vector<solution> population = generate_initial_population(initial_circuit, population_size);
	population = shift_population(population, STARTING_CITY_INDEX);
	calculate_fitness(population);
	calculate_mass(population);
	generate_initial_velocity(population);

	//main phase
	int nr_iterations = 0;
	best_ever = population[0];
	display_infos(population, nr_iterations, INFO_DISPLAY_MODE);
	while (stop_condition(nr_iterations) == 0)
	{
		//print_population(population);

		//update g k k_best
		g = calculate_g(g_initial, g_end, nr_iterations);
		k = calculate_k(k_initial, k_end, nr_iterations);

		if (population.size() < 2)
		{
			//do nothing.
		}
		else
		{
			sort(population.begin(), population.end(), sort_function_ascending_fitness);//ascending fitness. lower is better (fitness==tour distance)
		}

		k_best_solutions = find_k_best(population, int(k));

		//dependent movement operator
			//Calculate lij (acceleration) generated by each agent j from Kbest into each agent i
			//mtmns
		calculate_acceleration_k_best(population, k_best_solutions, g);
		int x = int(1.0 * population.size() * (PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION / 100.0));
		for (unsigned int i = x; i < population.size(); i++)
		{
			population[i].copy(solve_multi_target_movement_in_neighbourhood_space(population[i], k_best_solutions));
		}

		//independent movement operator
		calculate_velocity(population);//asta sa il mai fac o data 
		x = int(1.0 * population.size() * (PERCENTAGE_IGNORED_POPULATION_WHEN_VEL_CALCULATION / 100.0));
		//the next velocity of the ith agent is calculated as a fraction of its current velocity added to its acceleration
		for (unsigned int i = x; i < population.size(); i++)
		{
			population[i].copy(solve_modified_2_opt(population[i], nr_iterations));
			cout << i << " ";
		}

		population = shift_population(population, STARTING_CITY_INDEX);// so that all routes have starting city as first in vector
		//evaluate new fitness
		calculate_fitness(population);
		//calculate new mass
		calculate_mass(population);

		solution possible_best_ever;
		possible_best_ever = find_best(population);
		if (possible_best_ever.check_if_better(best_ever) == 1)
		{
			best_ever.copy(possible_best_ever);
		}

		nr_iterations = nr_iterations + 1;
		display_infos(population, nr_iterations, INFO_DISPLAY_MODE);
	}//end while nr iterations

	population.push_back(best_ever);

	if (population.size() < 2)
	{
		//do nothing
	}
	else
	{
		sort(population.begin(), population.end(), sort_function_ascending_fitness);
	}

	vector<solution> population_pre_final_2opt = population;
	if (PRINT_PRE_FINAL_2_OPT_VALUES == 1)
	{
		//print mean and best pre 2opt to file
		double total_length_mean = 0;
		for (unsigned int i = 0; i < population.size(); i++)//calculate mean
		{
			total_length_mean = total_length_mean + calculate_total_distance(population[i].points);
		}
		total_length_mean = total_length_mean / population.size();

		outfile << "pre final 2-opt:";
		outfile << " best_length = " << calculate_total_distance(population[0].points);
		outfile << " best_length _centers = " << calculate_total_distance_centers(population[0].points);
		outfile << " total_length_mean = " << total_length_mean << endl;
		cout << "pre final 2-opt:";
		cout << " best_length = " << calculate_total_distance(population[0].points);
		cout << " best_length _centers = " << calculate_total_distance_centers(population[0].points);
		cout << " total_length_mean = " << total_length_mean << endl;
	}

	//final 2-opt for population
	cout << "final 2-opt: ";
	int n = (PERCENTAGE_2_OPT / 100.0) * population.size();
	for (unsigned int i = 0; i < n; i++)
	{
		population[i].copy(solve_2_opt(population[i]));
		cout << i << " ";
	}
	cout << endl;
	calculate_fitness(population);

	if (population.size() < 2)
	{
		//do nothing
	}
	else
	{
		sort(population.begin(), population.end(), sort_function_ascending_fitness);
	}
	vector<solution> population_post_final_2opt = population;

	if (PRINT_POST_FINAL_2_OPT_VALUES == 1)
	{
		//print mean and best pre 2opt to file
		double total_length_mean = 0;
		for (unsigned int i = 0; i < population.size(); i++)//calculate mean
		{
			total_length_mean = total_length_mean + calculate_total_distance(population[i].points);
		}
		total_length_mean = total_length_mean / population.size();

		outfile << "post final 2-opt:";
		outfile << " best_length = " << calculate_total_distance(population[0].points);
		outfile << " best_length _centers = " << calculate_total_distance_centers(population[0].points);
		outfile << " total_length_mean = " << total_length_mean << endl;
	}



	//output stuff
	display_infos(population, nr_iterations, INFO_DISPLAY_MODE);
	/*
	result.copy(population[0]);
	for (unsigned int i = 0; i < population.size(); i++)
	{
		if (result.check_if_better(population[i]) == 0)
		{
			result.copy(population[i]);
		}
	}
	*/
	result.copy(population[0]);
	return result;
}




string test_settings_to_string()
{
	string result = "";
	result = "FILE = " + FILE_PATH + FILE_NAME + "\n" +
		"UNIQUE_ID = " + to_string(static_cast<long long>(unique_id)) + "\n" +
		"FLOAT_EQ_CT = " + to_string(static_cast<long double>(FLOAT_EQ_CT)) + "\n" +
		"STARTING_CITY_INDEX = " + to_string(static_cast<long long>(STARTING_CITY_INDEX)) + "\n" +
		"SEED = " + to_string(static_cast<long long>(SEED)) + "\n" +
		"NR_TEST_RUNS = " + to_string(static_cast<long long>(NR_TEST_RUNS)) + "\n" +
		"MAX_NR_ITERATIONS = " + to_string(static_cast<long long>(MAX_NR_ITERATIONS)) + "\n" +
		"POPULATION_SIZE = " + to_string(static_cast<long long>(POPULATION_SIZE)) + "\n" +
		"PERCENTAGE_2_OPT = " + to_string(static_cast<long long>(PERCENTAGE_2_OPT)) + "\n" +
		"PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = " + to_string(static_cast<long long>(PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION)) + "\n" +
		"PERCENTAGE_IGNORED_POPULATION_WHEN_VEL_CALCULATION = " + to_string(static_cast<long long>(PERCENTAGE_IGNORED_POPULATION_WHEN_VEL_CALCULATION)) + "\n" +
		"K_INITIAL = " + to_string(static_cast<long long>(K_INITIAL)) + "\n" +
		"K_END = " + to_string(static_cast<long long>(K_END)) + "\n" +
		"G_INITIAL = " + to_string(static_cast<long double>(G_INITIAL)) + "\n" +
		"G_END = " + to_string(static_cast<long double>(G_END)) + "\n" +
		"EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS = " + to_string(static_cast<long long>(EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.first)) + " " +
		to_string(static_cast<long long>(EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.second)) + "\n" +
		"MINIMUM_2OPT_MOVEMENTS = " + to_string(static_cast<long long>(MINIMUM_2OPT_MOVEMENTS)) + "\n" +
		"MINIMUM_2OPT_MOVEMENTS_INCREASE_EVERY_X_ITERATIONS = " + to_string(static_cast<long long>(MINIMUM_2OPT_MOVEMENTS_INCREASE_EVERY_X_ITERATIONS)) + "\n" +
		"VELOCITY_DECREASE_FACTOR = " + to_string(static_cast<long double>(VELOCITY_DECREASE_FACTOR)) + "\n" +
		"VELOCITY_RAND_LIMIT_INITIAL_CALCULATION = " + to_string(static_cast<long long>(VELOCITY_RAND_LIMIT_INITIAL_CALCULATION)) + "\n" +
		"VELOCITY_INCREASE_FACTOR_FOR_ACC = " + to_string(static_cast<long double>(VELOCITY_INCREASE_FACTOR_FOR_ACC)) + "\n" +
		"VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = " + to_string(static_cast<long double>(VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION)) + "\n" +
		"VELOCITY_CALCULATOR_SELECTION = " + to_string(static_cast<long long>(VELOCITY_CALCULATOR_SELECTION)) + "\n" +
		"INITIAL_VELOCITY_GENERATION_MODE = " + to_string(static_cast<long long>(INITIAL_VELOCITY_GENERATION_MODE)) + "\n" +
		"NR_K_MEMBERS_INFLUENCING_SOLUTION = " + to_string(static_cast<long long>(NR_K_MEMBERS_INFLUENCING_SOLUTION)) + "\n" +
		"USE_RANDOM_STARTING_POINTS_2_OPT = " + to_string(static_cast<long long>(USE_RANDOM_STARTING_POINTS_2_OPT)) + "\n" +
		"CALCULATE_MASS_W_MULTIPLICATION_FACTOR = " + to_string(static_cast<long double>(CALCULATE_MASS_W_MULTIPLICATION_FACTOR)) + "\n" +
		"MASS_MULTIPLICATION_FACTOR_INDEPENDENT = " + to_string(static_cast<long double>(MASS_MULTIPLICATION_FACTOR_INDEPENDENT)) + "\n" +
		"VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = " + to_string(static_cast<long double>(VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION)) + "\n" +
		"PRINT_PRE_FINAL_2_OPT_VALUES = " + to_string(static_cast<long long>(PRINT_PRE_FINAL_2_OPT_VALUES)) + "\n" +
		"PRINT_POST_FINAL_2_OPT_VALUES = " + to_string(static_cast<long long>(PRINT_POST_FINAL_2_OPT_VALUES)) + "\n" +
		"PRINTS_FINAL_CIRCUIT = " + to_string(static_cast<long long>(PRINT_FINAL_CIRCUIT)) + "\n" +
		"RESTRICTED_CANDIDATE_LIST_SIZE = " + to_string(static_cast<long long>(RESTRICTED_CANDIDATE_LIST_SIZE)) + "\n" +
		"INITIAL_GENERATION_PROCEDURE = " + to_string(static_cast<long long>(INITIAL_GENERATION_PROCEDURE)) + "\n" +
		"INFO_DISPLAY_MODE = " + to_string(static_cast<long long>(INFO_DISPLAY_MODE)) + "\n" +
		"MTMNS_INFLUENCER_SELECTION = " + to_string(static_cast<long long>(MTMNS_INFLUENCER_SELECTION)) + "\n" +
		"SMALL_MOVE_OPERAND_USED = " + to_string(static_cast<long long>(SMALL_MOVE_OPERAND_USED)) + "\n" +
		"SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT = " + to_string(static_cast<long double>(SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT)) + "\n";

	return result;
}











void run_test_suite(string FILE_PATH, string FILE_NAME, int unique_id)
{
	//technically am o probelma ca pot avea 2 solutii identice (acelasi traseu parcurs in cele 2 sensuri care le vede ca fiind
		//diferite. not ok dar macar e un caz mic

	vector<point_2d> initial_points;

	ifstream inFile;

	inFile.open(FILE_PATH + FILE_NAME);

	if (!inFile) {
		cerr << "Unable to open file datafile.txt";
		exit(1);   // call system to stop
	}

	string temp;
	vector <point_2d> points;
	point_2d temp_point;

	inFile >> temp; //index 
	while (temp != "EOF")
	{
		temp_point.index = stoi(temp);
		inFile >> temp;//x
		temp_point.x = stof(temp);
		inFile >> temp;//y
		temp_point.y = stof(temp);
		inFile >> temp;//radius
		temp_point.radius = stof(temp);
		temp_point.x_circuit = temp_point.x;
		temp_point.y_circuit = temp_point.y;
		initial_points.push_back(temp_point);
		inFile >> temp; //index 
	}


	outfile.open(FILE_PATH + "results\\" + FILE_NAME + to_string(static_cast<long long>(unique_id)) + "_results.txt");

	cout << "STARTING RUN for file " << FILE_NAME << " UNIQUE ID = " << unique_id << endl << endl << endl;
	outfile << "STARTING RUN for file " << FILE_NAME << " UNIQUE ID = " << unique_id << endl << endl << endl;

	cout << test_settings_to_string() << endl << endl;
	outfile << test_settings_to_string() << endl << endl;

	vector<double> results;
	for (int i = 0; i < NR_TEST_RUNS; i++)
	{
		cout << endl << endl << endl << "RUN " << i << " OF " << NR_TEST_RUNS - 1 << endl;
		outfile << endl << endl << endl << "RUN " << i << " OF " << NR_TEST_RUNS - 1 << endl;
		//dgsa
		solution solution0;
		solution0 = solve_dgsa_tsp(initial_points, POPULATION_SIZE, K_INITIAL, K_END, G_INITIAL, G_END);
		outfile << "final solution: ";
		outfile << "best_length = " << calculate_total_distance(solution0.points);
		outfile << " best_length_center = " << calculate_total_distance_centers(solution0.points) << endl << endl;
		if (PRINT_FINAL_CIRCUIT == 1)
		{
			print_circuit(solution0.points);
		}
		results.push_back(calculate_total_distance(solution0.points));
	}
	double mean = 0;
	for (int i = 0; i < results.size(); i++)
	{
		mean = mean + results[i];
	}
	mean = mean / results.size();

	map<double, int> histogram_results;
	//calculate + print histogram
	cout << "FOR TEST SUITE : \n\nPRINTING RESULTS HISTOGRAM\n";
	outfile << "FOR TEST SUITE : \n\nPRINTING RESULTS HISTOGRAM\n";
	for (int i = 0; i < results.size(); i++)
	{
		histogram_results[int(floor(results[i]))] += 1;
	}
	int  ok = 0;
	int best = 0;
	vector<double> results_percentage_histogram;//v[0]=0%err fata de best;v[1]=under 1%; ca procente
	for (int k = 0; k < 101; k++)
	{
		results_percentage_histogram.push_back(0);
	}
	for (auto elem = histogram_results.cbegin(); elem != histogram_results.cend(); elem++)
	{
		if (ok == 0)
		{
			best = int(floor(elem->first));
			ok = 1;
		}
		//calculate error;
		double error = 0;
		error = (abs(elem->first - best) / best) * 100;//relative error
		if (error > 100)
		{
			results_percentage_histogram[100] += elem->second;
		}
		else
		{
			results_percentage_histogram[ceil(error)] += elem->second;
		}



		cout << elem->first << " : " << elem->second << "\n";
		outfile << elem->first << " : " << elem->second << "\n";
	}

	//between  0 an 1 % difference compared to best, i find results_percentage_histogram % results
	cout << "RESULTS_PERCENTAGE_DIFFERENCE_HISTOGRAM" << endl;
	outfile << "RESULTS_PERCENTAGE_DIFFERENCE_HISTOGRAM" << endl;
	for (int k = 0; k < 101; k++)
	{
		results_percentage_histogram[k] = (results_percentage_histogram[k] / results.size()) * 100;
		cout << "(" << k - 1 << ":" << k << "] % : " << results_percentage_histogram[k] << " %" << endl;
		outfile << "(" << k - 1 << ":" << k << "] % : " << results_percentage_histogram[k] << " %" << endl;
	}


	//final prints
	cout << "BEST LENGTH CENTER MEAN OVER " << NR_TEST_RUNS << " TESTS IS " << mean << endl;
	outfile << "BEST LENGTH CENTER MEAN OVER " << NR_TEST_RUNS << " TESTS IS " << mean << endl;

	cout << "FINISHED RUN" << endl << endl << endl;
	outfile << "FINISHED RUN" << endl << endl << endl;
	outfile.close();

}




void proces_input_arguments(int argc, char *argv[])
{
	switch (argc)
	{
	case(1):
	{
		FILE_PATH = "";
		FILE_NAME = ""berlin52.tsp";
		unique_id = 0;

		FLOAT_EQ_CT = 0.0001;
		FLOAT_EQ_CT_CIRCUIT = 1;
		STARTING_CITY_INDEX = 0;

		SEED = 0;
		NR_TEST_RUNS = 10;
		MAX_NR_ITERATIONS = 200;
		MAX_NR_ITERATIONS_RBA = 1000;
		POPULATION_SIZE = 10;//poate sa il fac sa depinda de nr points

		PERCENTAGE_2_OPT = 100;//does 2opt over best % part of population
		PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = 0;//best part of population is not changing
		PERCENTAGE_IGNORED_POPULATION_WHEN_VEL_CALCULATION = 0;

		K_INITIAL = 5;
		K_END = 3;
		G_INITIAL = 0.5;
		G_END = 0.1;

		EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.first = MAX_NR_ITERATIONS / 5;
		EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.second = 10000;//10000 //minimum of 2opt movements
		MINIMUM_2OPT_MOVEMENTS = 1;//asta sa il variez => arat ca afecteaza si ca ideal e 1|3 ca sa nu fie prea mult pt ca timp
		MINIMUM_2OPT_MOVEMENTS_INCREASE_EVERY_X_ITERATIONS = MAX_NR_ITERATIONS / 1;//after this many iterations make one more min movement
		VELOCITY_INCREASE_FACTOR_FOR_ACC = 1;//10;//usually 1
		VELOCITY_DECREASE_FACTOR = 2;//usually 1
		VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = 1;
		VELOCITY_RAND_LIMIT_INITIAL_CALCULATION = 40;//used w/ INITIAL_VELOCITY_GENERATION_MODE==0|2

		MASS_MULTIPLICATION_FACTOR_INDEPENDENT = 1;// usually 1
		NR_K_MEMBERS_INFLUENCING_SOLUTION = 2;//max nr possible ; usually K_INITIAL; used when i want a low number from the start
		RESTRICTED_CANDIDATE_LIST_SIZE = 10; // for INITIAL_GENERATION_PROCEDURE == 2
		SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT = 2.5; // used with SMALL_MOVE_OPERAND_USED== 2 || 3; small nr=>lower limit

		USE_RANDOM_STARTING_POINTS_2_OPT = 1;//0=no; 1=yes
		CALCULATE_MASS_W_MULTIPLICATION_FACTOR = 0;//0=no; 1=yes


		//pt INITIAL_GENERATION_PROCEDURE: 
		//0=random; 
		//1=nearest neighbour cu random start point; 
		//2=random nearest nighbour din RESTRICTED_CANDIDATE_LIST_SIZE best
		INITIAL_GENERATION_PROCEDURE = 2;//deafult 2

		//0 = random % VELOCITY_RAND_LIMIT_INITIAL_CALCULATION
		//1 = random % largest distance between solutions
		//2 = fixed at VELOCITY_RAND_LIMIT_INITIAL_CALCULATION
		INITIAL_VELOCITY_GENERATION_MODE = 1;//deafult 1

		//0 = uses acc_mean for velocity calculation
		//1 = uses acc_total for velocity calculation
		VELOCITY_CALCULATOR_SELECTION = 1;//default 1;

		//closeness metric used: also responsible for the make small move operand
		//0 = metric = swap/transposition; small_move = swap/transposition
		//1 = metric = nr identical edges in conectivity matrix (how many identical connections 2 solutions have);small move = swap/transposition
		//2 = same as case 0 but also takes fitness into account. does not allow big fitness degradations if possible; prefers moves that dont affect length a lot
		//3 = same as case 2 but refuses to make a move that degrades fitness a lot 
		//4 = metric = nr identical edges; small move = remove point and reinsert
		SMALL_MOVE_OPERAND_USED = 3; // default 0 ; 3 merge best

		//multi_target_movement_in_neighbourhood_space selection
		// 0 = all used, best solution used last
		// 1 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; does not allow double influences
		// 2 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; allows double influences
		// 3 = same as 2 but makes sure some movement is made if possible
		MTMNS_INFLUENCER_SELECTION = 3;//default 0

		//pt info display mode: 0=basic-length; 1=length+acc+vel(total); 2: length+acc/vel mean; 3: no display
		INFO_DISPLAY_MODE = 1;//deafult 1

		PRINT_FINAL_CIRCUIT = 1;
		PRINT_PRE_FINAL_2_OPT_VALUES = 1;//0=no; 1=yes
		PRINT_POST_FINAL_2_OPT_VALUES = 1;//0=no; 1=yes

		break;
	}

	case(45):
	{
		FILE_PATH = argv[1];
		FILE_NAME = argv[2];
		unique_id = atoi(argv[3]);

		FLOAT_EQ_CT = atof(argv[4]);
		STARTING_CITY_INDEX = atoi(argv[5]);

		SEED = atoi(argv[6]);
		NR_TEST_RUNS = atoi(argv[7]);
		MAX_NR_ITERATIONS = atoi(argv[8]);
		POPULATION_SIZE = atoi(argv[9]);

		PERCENTAGE_2_OPT = atoi(argv[10]);
		PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = atoi(argv[11]);
		PERCENTAGE_IGNORED_POPULATION_WHEN_VEL_CALCULATION = atoi(argv[12]);
		//insert "here0" here in cmd
		K_INITIAL = atof(argv[14]);
		K_END = atof(argv[15]);
		G_INITIAL = atof(argv[16]);
		G_END = atof(argv[17]);
		//insert "here1" here in cmd
		EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.first = atoi(argv[19]);
		EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.second = atoi(argv[20]);
		MINIMUM_2OPT_MOVEMENTS = atoi(argv[21]);
		MINIMUM_2OPT_MOVEMENTS_INCREASE_EVERY_X_ITERATIONS = atoi(argv[22]);
		VELOCITY_INCREASE_FACTOR_FOR_ACC = atof(argv[23]);
		VELOCITY_DECREASE_FACTOR = atof(argv[24]);
		VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = atof(argv[25]);
		VELOCITY_RAND_LIMIT_INITIAL_CALCULATION = atoi(argv[26]);
		//insert "here2" here in cmd
		MASS_MULTIPLICATION_FACTOR_INDEPENDENT = atof(argv[28]);
		NR_K_MEMBERS_INFLUENCING_SOLUTION = atoi(argv[29]);
		RESTRICTED_CANDIDATE_LIST_SIZE = atoi(argv[30]);
		SMALL_MOVE_FITNESS_DEGRADATION_ACCEPTANCE_LIMIT = atof(argv[31]);
		USE_RANDOM_STARTING_POINTS_2_OPT = atoi(argv[32]);
		CALCULATE_MASS_W_MULTIPLICATION_FACTOR = atoi(argv[33]);
		//insert "here3" here in cmd
		INITIAL_GENERATION_PROCEDURE = atoi(argv[35]);
		INITIAL_VELOCITY_GENERATION_MODE = atoi(argv[36]);
		VELOCITY_CALCULATOR_SELECTION = atoi(argv[37]);
		SMALL_MOVE_OPERAND_USED = atoi(argv[38]);
		MTMNS_INFLUENCER_SELECTION = atoi(argv[39]);
		//insert "here4" here in cmd
		INFO_DISPLAY_MODE = atoi(argv[41]);
		PRINT_FINAL_CIRCUIT = atoi(argv[42]);
		PRINT_PRE_FINAL_2_OPT_VALUES = atoi(argv[43]);
		PRINT_POST_FINAL_2_OPT_VALUES = atoi(argv[44]);

		break;
	}
	default:
	{
		cout << "ERROR: improper number of arguments used";
		break;
	}

	}





}



int main(int argc, char *argv[])
{

	proces_input_arguments(argc, argv);


	srand(SEED);

	run_test_suite(FILE_PATH, FILE_NAME, unique_id);

	
	getchar();

	return 0;
}







