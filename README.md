# DGSA-CETSP

Implementation of DGSA+RBA(modified) for solving CETSP

Code is given as is, without any warranties


Based on paper "A discrete gravitational search algorithm for solving combinatorial optimization problems".
https://www.sciencedirect.com/science/article/pii/S0020025513006762
and previous implementation for tsp: https://gitlab.com/cv22/DGSA-TSP

Parameter descriptions can be found inside the code.

Code is not written "cleanly". I know.




